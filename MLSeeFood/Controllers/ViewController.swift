//
//  ViewController.swift
//  MLSeeFood
//
//  Created by Jeerapat Sripumngoen on 26/6/2563 BE.
//  Copyright © 2563 Jeerapat Sripumngoen. All rights reserved.
//

import UIKit
import CoreML
import Vision

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    let uiImagePicker = UIImagePickerController()
    let uiNavigation = UINavigationController()
        
    @IBOutlet weak var displayImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        uiNavigation.delegate = self
        uiImagePicker.delegate = self
        
        uiImagePicker.sourceType = .photoLibrary
        uiImagePicker.allowsEditing = true
    }

    @IBAction func cameraAction(_ sender: UIBarButtonItem) {
        
        print("camera action!")
         
        present(uiImagePicker, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let pickedImage = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage
        
        let ciImage = CIImage(image: pickedImage!)
        
        processImage(ciImage: ciImage!)
        
        displayImageView.image = pickedImage

        uiImagePicker.dismiss(animated: true, completion: nil)
    }
    
    func processImage(ciImage: CIImage) {
        if let model = try? VNCoreMLModel(for: Inceptionv3().model) {
            let request = VNCoreMLRequest(model: model) { (request, error) in

                let results = request.results as? [VNClassificationObservation]
                
                print(results)
                
                if let sureAnswer = results?.first {
                    if (sureAnswer).identifier.contains("hotdog") {
                        self.navigationItem.title = "Hotdog!"
                    }
                    else {
                        self.navigationItem.title = "Not hotdog!"
                    }
                }

            }
            
            let handler = VNImageRequestHandler(ciImage: ciImage)
            
            do {
                try handler.perform([request])
            } catch {
                print(error)
            }
        }
        else {
            print("can not load model")
        }
    }
    
}

